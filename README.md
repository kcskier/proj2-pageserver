# Project 2 Web Server with Flask

# Getting Things Running

* Everything needed to get this running should be in the web folder. This includes a Dockerfile and a run script
* Dockerfile can be found in the /web folder
* html files can be found in the /web/templates folder
* the run.sh script will build and run the web server inside a docker file. It will also specify a port to run the server on.

# Tasks

* The goal of this project is to implement the same "file checking" logic that you implemented in project 1 using flask. 

* Like project 1, if a file ("name.html") exists, transmit "200/OK" header followed by that file html. If the file doesn't exist, transmit an error code in the header along with the appropriate page html in the body. You'll do this by creating error handlers taught in class (refer to the slides; it's got all the tricks needed). You'll also create the following two html files with the error messages. 
    * "404.html" will display "File not found!"
    * "403.html" will display "File is forbidden!"
    
* Update your name and email in the Dockerfile.

* You will submit your credentials.ini in canvas. It should have information on how we should get your Dockerfile and your git repo. 

# Grading Rubric
* If your code works as expected: 100 points.

* For every wrong functionality (i.e., (a), (b), and (c) from project 1), 20 points will be docked off. 

* If none of the functionalities work, 40 points will be given assuming 
    * the credentials.ini is submitted with the correct URL of your repo, 
    * the Dockerfile builds without any errors, and 
    * if the two html files (404.html and 403.html) are created in the appropriate location. 
    
* If the Dockerfile doesn't build or is missing, 20 points will be docked off.

* If the two html files are missing, 20 points will be docked off.

* If credentials.ini is missing, 0 will be assigned.

# Who do I talk to? ###

* Original Assignment Provided and Maintained by Ram Durairajan, Steven Walton.
* Additional Functionality added by Kendell Crocker