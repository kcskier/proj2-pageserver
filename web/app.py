from flask import Flask, render_template, abort, Response

app = Flask(__name__)

def formatLoc(text):
    text = "templates/" + text
    return text


@app.route('/<path:subpath>')
def subpath(subpath):

#defines a location string, this is more for debugging purposes
    location = formatLoc(subpath)
    print("Subpath is", subpath)
    print("Location is", location)
    subpath = "/" + subpath
    
#Checks if the file is forbidden    
    if "//" in subpath:
        return forbidden(403)
    if ".." in subpath:
        return forbidden(403)
    if "*" in subpath:
        return forbidden(403)
    if "~" in subpath:
        return forbidden(403)
    
#This will return a page if it exists in the templates folder, if not it returns a 404 error
    try:
        return render_template(subpath), 200
    except:
        print("if you see this, then the file", location, "does not exist")
        return notfound(404)

#Error Handlers for 403 and 404
@app.errorhandler(403)
def forbidden(e):
    Response(status=403)
    return render_template("forbidden.html"), 403

@app.errorhandler(404)
def notfound(e):
    Response(status=404)
    return render_template("page_not_found.html"), 404
    

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0',port=5000)